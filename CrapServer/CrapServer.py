import socket
import sys

from ClientThread import ClientThread
from BroadcastThread import BroadcastThread

print "Hello, i am CrapServer. Welcome to CrapChat"

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

server_address = ('192.168.2.102', 10000)
print >>sys.stderr, "starting TCP socket on %s port %s" % server_address

sock.bind(server_address)
print "socket bound, waiting for clients"

sock.listen(1)

broadcastThread = BroadcastThread()
broadcastThread.start()

while True:
        connection, client_address = sock.accept()

        broadcastThread.addClient(client_address)

        ClientThread(connection, client_address, broadcastThread).start()