import threading
import sys
import socket


class BroadcastThread(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        self.clients = []

        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

    def run(self):
        server_address = ('', 10001)
        print >> sys.stderr, "starting UDP socket on %s port %s" % server_address

        self.sock.bind(server_address)

        data, address = self.sock.recvfrom(1024)

        print "received something ''", data

    def addClient(self, address):
        self.clients.append(address)
        print >> sys.stderr, self.clients

    def broadcast(self, message):
        print "Broadcasting message: '%s'" % message

        for address in self.clients:
            self.sock.sendto(message, (address[0], 10001))
            print "sent to ", address[0]