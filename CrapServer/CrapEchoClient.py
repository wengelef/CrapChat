import socket
import sys
import time

# Create TCP/IP Socket
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Connect Socket to the port where the server is listening
server_address = ('localhost', 10000)

print >>sys.stderr, "connecting with CrapServer at %s port %s" % server_address
sock.connect(server_address)

try:
    # Send Data
    message = "CrapServer riecht ganz streng nach mist"
    print >>sys.stderr, "sending message: %s" % message
    print >>sys.stderr, "length %s" % len(message)

    # send length
    sock.sendall(str(len(message)))

    # wait for "ok"
    sock.recv(1024)
    print "got ok"

    # send message
    sock.sendall(message)

    data = sock.recv(len(message))
    print >>sys.stderr, "received %s" % data

finally:
    print >>sys.stderr, "closing socket"