import threading
import sys


class ClientThread(threading.Thread):
    def __init__(self, socket, address, broadcastThread):
        threading.Thread.__init__(self)
        self.sock = socket
        self.address = address
        self.broadcastThread = broadcastThread

    def run(self):
        print >> sys.stderr, "Got CrapClient ", self.address

        while True:
            print >> sys.stderr, "Waiting for Size"

            # receive length
            size = self.sock.recv(1024)

            if size.strip():
                print >> sys.stderr, "Got length %s" % size

                # send "ok"
                self.sock.send("ok")

                # receive message
                data = self.sock.recv(int(size))

                if data:
                    print "Got Message %s" % data
                    print >> sys.stderr, "sending data back to client."
                    self.sock.sendall(data)

                    self.broadcastThread.broadcast(data)

                    print "done"

            else:
                print >> sys.stderr, "Corrupt data, ignore request."
