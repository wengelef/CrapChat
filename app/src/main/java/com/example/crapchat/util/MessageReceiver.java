package com.example.crapchat.util;

import android.support.annotation.NonNull;

import com.example.crapchat.model.Message;

/**
 * Created by Flo on 29.05.2016.
 */
public interface MessageReceiver {
    void onReceiveMessage(@NonNull final Message message);
}
