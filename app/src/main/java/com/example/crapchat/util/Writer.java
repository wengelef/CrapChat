package com.example.crapchat.util;

import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.example.crapchat.model.Message;
import com.example.crapchat.model.User;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

import timber.log.Timber;

/**
 * Created by Flo on 29.05.2016.
 */
public class Writer {

    @NonNull
    private final PrintWriter mWriter;

    @NonNull
    private final BufferedReader mReader;

    // Replace with User or something
    @NonNull
    private final User mUser;

    @NonNull
    private final MessageReceiver mMessageReceiver;

    public Writer(@NonNull Socket socket, @NonNull User user, @NonNull MessageReceiver messageReceiver) throws IOException {
        mWriter = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), true);
        mReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));

        mUser = user;
        mMessageReceiver = messageReceiver;
    }

    public String read() {
        try {
            String message = "";
            int charsRead = 0;
            char[] buffer = new char[1064];

            charsRead = mReader.read(buffer);

            if (charsRead != -1) {
                message = new String(buffer).substring(0, charsRead);
            }

            return message;
        } catch (IOException e) {
            return "Error receiving response:  " + e.getMessage();
        }
    }

    public void write(@NonNull Message message) {
        new WriterTask().execute(message);
    }

    private Message writeInternal(Message message) {
        Timber.d("Sending msg %s", message.getMessage());
        // Send size

        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();

        String messageJson = gson.toJson(message);

        Timber.i(messageJson);

        Timber.d("Sending length %d", messageJson.length());
        mWriter.println(String.valueOf(messageJson.length()));

        // Get "ok"
        if (read().equals("ok")) {
            Timber.d("got ok");

            // Send Message
            mWriter.println(messageJson);

            // Get Response
            return gson.fromJson(read(), Message.class);
        } else {
            Timber.d("didn't get ok");
        }
        return null;
    }

    public class WriterTask extends AsyncTask<Message, Void, Message> {

        @Override
        protected Message doInBackground(Message... messages) {
            return writeInternal(messages[0]);
        }
    }
}
