package com.example.crapchat.util;

import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.example.crapchat.model.Message;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;

/**
 * Created by Flo on 29.05.2016.
 */
public class BroadcastReceiver extends AsyncTask<Integer, Void, Void> {

    @NonNull
    private final MessageReceiver mMessageReceiver;

    public BroadcastReceiver(@NonNull MessageReceiver messageReceiver) {
        mMessageReceiver = messageReceiver;
    }

    @Override
    protected Void doInBackground(Integer... integers) {
        DatagramSocket mDatagramSocket = null;

        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();
        try {
            mDatagramSocket = new DatagramSocket(integers[0]);
            mDatagramSocket.setReuseAddress(true);
            mDatagramSocket.setBroadcast(true);

            while (true) {
                byte[] broadcast = new byte[1024];
                DatagramPacket dp = new DatagramPacket(broadcast, broadcast.length);

                    /* debug Echo
                    try {
                        byte[] echo = new byte[1];
                        echo[0] = Integer.valueOf(1).byteValue();
                        datagramSocket.send((new DatagramPacket(echo, echo.length, mSocket.getInetAddress(), UDP_PORT)));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                     */

                try {
                    mDatagramSocket.receive(dp);
                    final String msg = new String(broadcast).substring(0, dp.getLength());
                    mMessageReceiver.onReceiveMessage(gson.fromJson(msg, Message.class));
                } catch (IOException e) {
                    e.printStackTrace();
                    break;
                }
            }
        } catch (SocketException e) {
            e.printStackTrace();
        }
        return null;
    }
}