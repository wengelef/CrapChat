package com.example.crapchat;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.example.crapchat.model.Message;
import com.example.crapchat.model.User;
import com.example.crapchat.util.BroadcastReceiver;
import com.example.crapchat.util.MessageReceiver;
import com.example.crapchat.util.Writer;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Flo on 28.05.2016.
 */
public class ChatFragment extends Fragment {

    // TODO this
    private static final String SERVER_IP = "192.168.2.102";
    private static final int TCP_PORT = 10000;
    private static final int UDP_PORT = 10001;

    public static final String KEY_IP_ADDRESS = "key_ip_address";
    public static final String KEY_NAME = "key_name";

    private Socket mSocket;

    private Writer mWriter;

    @NonNull
    private User mUser;

    @Nullable
    private MessageReceiver mMessageReceiver;

    @BindView(R.id.ipAddress)
    TextView mIpAddressTextView;

    @BindView(R.id.text_receive)
    TextView mResponseTextView;

    @BindView(R.id.text_send)
    TextView mSendTextView;

    @BindView(R.id.message_input)
    EditText mMessageEditor;

    public static ChatFragment newInstance(String ipAddress, String name) {
        Bundle args = new Bundle(2);
        args.putString(KEY_IP_ADDRESS, ipAddress);
        args.putString(KEY_NAME, name);

        ChatFragment fragment = new ChatFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fr_chat, container, false);
        ButterKnife.bind(this, root);
        return root;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // TODO this
        //mServerIp = SERVER_IP;

        if (getArguments().containsKey(KEY_IP_ADDRESS) && getArguments().containsKey(KEY_NAME)) {

            mUser = new User(getArguments().getString(KEY_NAME), getArguments().getString(KEY_IP_ADDRESS));

            if (mUser.getAddress() != null) {
                mIpAddressTextView.setText(mUser.getAddress());
            } else if (BuildConfig.DEBUG) {
                // Emulator BS
                mUser.setAddress("10.0.2.2");
                mUser.setName("Emulator");
                mIpAddressTextView.setText(mUser.getAddress());
            } else {
                mIpAddressTextView.setText("No valid CrapServerIP");
            }

            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        InetAddress serverAddress = InetAddress.getByName(SERVER_IP);
                        mSocket = new Socket(serverAddress, TCP_PORT);

                        new BroadcastReceiver(getMessageReceiver()).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, UDP_PORT);

                        mWriter = new Writer(mSocket, mUser, getMessageReceiver());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }).start();
        }
    }

    @NonNull
    private MessageReceiver getMessageReceiver() {
        if (mMessageReceiver == null) {
            mMessageReceiver = new MessageReceiver() {
                @Override
                public void onReceiveMessage(@NonNull final Message message) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mResponseTextView.setText(String.format("%s%s%s", mResponseTextView.getText().toString(), "\r\n", message.getFormattedChatLine()));
                        }
                    });
                }
            };
        }
        return mMessageReceiver;
    }

    @Override
    public void onStop() {
        disconnect();
        super.onStop();
    }

    private void disconnect() {
        if (mSocket != null) {
            try {
                mSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @OnClick(R.id.button_send)
    void onSendClicked() {
        String msg = mMessageEditor.getText().toString().trim();
        mSendTextView.setText(String.format("%s%s%s", mSendTextView.getText().toString(), "\r\n", msg));
        mWriter.write(new Message(mUser, msg));
    }
}
