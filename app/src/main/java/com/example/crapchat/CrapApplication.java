package com.example.crapchat;

import android.app.Application;

import timber.log.Timber;

/**
 * Created by Flo on 29.05.2016.
 */
public class CrapApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }
    }
}
