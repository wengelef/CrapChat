package com.example.crapchat.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Flo on 29.05.2016.
 */
public class User {

    @SerializedName("Name")
    String name;

    @SerializedName("Address")
    String address;

    public User(String name, String address) {
        this.name = name;
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
