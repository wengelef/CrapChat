package com.example.crapchat.model;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Flo on 29.05.2016.
 */
public class Message {

    @SerializedName("User")
    private User user;

    @SerializedName("Message")
    private String message;

    public Message(@NonNull User user, @NonNull String message) {
        this.user = user;
        this.message = message;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @NonNull
    public String getFormattedChatLine() {
        if (user != null) {
            return String.format("%s (%s): %s", user.getName(), user.getAddress(), message);
        }
        return "";
    }
}
